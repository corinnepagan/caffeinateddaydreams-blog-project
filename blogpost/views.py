from django.shortcuts import render, redirect, get_object_or_404
from blogpost.models import Blogpost, Comment
from blogpost.forms import BlogpostForm, CommentForm
from django.contrib.auth.decorators import login_required


def view_posts(request):
    blogpost = Blogpost.objects.all()
    context = {
        "blogpost": blogpost,
    }
    return render(request, "blogpost/view_posts.html", context)

def show_post(request, id):
    blogpost = get_object_or_404(Blogpost, id=id)
    context = {
        "blogpost": blogpost
    }
    return render(request, "blogpost/show_post.html", context)

def create_post(request):
    if request.method == "POST":
        form = BlogpostForm(request.POST)
        if form.is_valid():
            post = form.save(False)
            post.author = request.user
            post.save
            return redirect("view_posts")

    else:
        BlogpostForm()

        context = {
            "post": post,
        }
        return render(request, "blogpost/create.html", context)

def postcomment(request, id):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(False)
            comment.author = request.user
            comment.save
            return redirect("show_post", id=id)

    else:
        CommentForm()
        context = {
            "comment": comment
        }
        return render(request, "blogpost/comment.html", context)
