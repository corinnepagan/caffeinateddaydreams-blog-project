from django.forms import ModelForm
from blogpost.models import Blogpost, Comment

class BlogpostForm(ModelForm):
    class Meta:
        model = Blogpost
        fields = (
            "title",
            "post",
        )

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = (
            "input",
        )
