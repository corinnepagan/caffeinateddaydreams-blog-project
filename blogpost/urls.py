from django.urls import path
from blogpost.views import show_post, create_post, postcomment, view_posts

urlpatterns = [
    path('', view_posts, name="view_posts"),
    path('<int:id>/', show_post, name="show_post"),
    path('create/', create_post, name="create_post"),
    path('comment/', postcomment, name="postcomment"),
    ]
