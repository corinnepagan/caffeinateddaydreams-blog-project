from django.db import models
from django.conf import settings

class Blogpost(models.Model):
    title = models.CharField(max_length=150)
    post = models.TextField()
    posted = models.DateField(auto_now_add=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="blogpost",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.title


class Comment(models.Model):
    input = models.CharField(max_length=500)
    posted = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(
        "Blogpost",
        related_name="comment",
        on_delete=models.CASCADE,
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="comment",
        on_delete=models.CASCADE,
    )
