from django.contrib import admin
from blogpost.models import Blogpost, Comment

@admin.register(Blogpost)
class BlogpostAdmin(admin.ModelAdmin):
    list_display = (
            "title",
            "id",
            "post",
    )

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = (
        "input",
        "id",
    )
